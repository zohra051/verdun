<?php
include("header.php");
?>

<!DOCTYPE html>
<html>
	<head>
		<link rel="stylesheet" href="css/espace-membre.css" type="text/css"/>
		<title>Accueil</title>
	</head>
	
	<body>
		<div class="regles"> 
			<br>
			<br>
			<h3>Règle du jeu </h3>
			<br>
			<ul>2 joueurs s'affrontent sur un plateau.</ul>
			<ul>A chaque tour, les joueurs choisissent le nombre de soldats partant à l'assaut, tous les soldats sont perdus.</ul>
			<ul>Le joueur qui a le plus de soldats remporte l'escarmouche et conquiert un territoire sur son adversaire.</ul>
			<ul>Le round se termine quand les deux joueurs n'ont plus de soldats à envoyer ou quand un joueur a atteint la base ennemie.</ul>
		</div>
	</body>
</html>