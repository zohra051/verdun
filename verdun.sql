-- phpMyAdmin SQL Dump
-- version 4.6.6deb4
-- https://www.phpmyadmin.net/
--
-- Client :  localhost:3306
-- Généré le :  Ven 11 Janvier 2019 à 01:00
-- Version du serveur :  10.1.37-MariaDB-0+deb9u1
-- Version de PHP :  7.0.33-0+deb9u1

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Base de données :  `verdun`
--

-- --------------------------------------------------------

--
-- Structure de la table `messages`
--

CREATE TABLE `messages` (
  `id` int(11) NOT NULL,
  `author` varchar(50) DEFAULT NULL,
  `content` text,
  `created_at` datetime DEFAULT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COMMENT='La table qui va contenir tous les messages voyons !';

--
-- Contenu de la table `messages`
--

INSERT INTO `messages` (`id`, `author`, `content`, `created_at`) VALUES
(140, 'Joueur', 'test', '2019-01-11 00:56:51'),
(139, 'Joueur', 'hello', '2019-01-11 00:52:40');

-- --------------------------------------------------------

--
-- Structure de la table `utilisateurs`
--

CREATE TABLE `utilisateurs` (
  `UTI_Id` int(11) NOT NULL,
  `UTI_Pseudo` text NOT NULL,
  `UTI_MDP` text NOT NULL,
  `UTI_Score` int(11) NOT NULL DEFAULT '100',
  `UTI_Droit` int(11) NOT NULL DEFAULT '1'
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Contenu de la table `utilisateurs`
--

INSERT INTO `utilisateurs` (`UTI_Id`, `UTI_Pseudo`, `UTI_MDP`, `UTI_Score`, `UTI_Droit`) VALUES
(2, 'aled', '5bc23e2ce9a4c61b4622f4a5edf3d10b', 0, 2),
(3, 'thomas', 'ef6e65efc188e7dffd7335b646a85a21', 20, 2),
(5, 'test', '098f6bcd4621d373cade4e832627b4f6', 10, 1),
(6, 'benoit', '0b36e58acc0a3c7f807b049c935f3b94', 50, 1),
(11, 'score', 'ca1cd3c3055991bf20499ee86739f7e2', 100, 1),
(12, 'antoine', '3f518a74f371046c1c74150197be919c', 100, 1),
(13, 'hello', '6057f13c496ecf7fd777ceb9e79ae285', 100, 1),
(14, 'heyyy', '098f6bcd4621d373cade4e832627b4f6', 100, 1);

--
-- Index pour les tables exportées
--

--
-- Index pour la table `messages`
--
ALTER TABLE `messages`
  ADD PRIMARY KEY (`id`);

--
-- Index pour la table `utilisateurs`
--
ALTER TABLE `utilisateurs`
  ADD PRIMARY KEY (`UTI_Id`);

--
-- AUTO_INCREMENT pour les tables exportées
--

--
-- AUTO_INCREMENT pour la table `messages`
--
ALTER TABLE `messages`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=141;
--
-- AUTO_INCREMENT pour la table `utilisateurs`
--
ALTER TABLE `utilisateurs`
  MODIFY `UTI_Id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=15;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
