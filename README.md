# Verdun

## Présentation

Un site web implémentant le jeu verdun, jouable en ligne, en local, ainsi qu'en IA.

## Comment installer le site

* Importer la base de données verdun.sql

* installer les paquets ws

```
    npm install ws
```

* lancer le serveur

```
    node server/server.js
```

## Contributeur

### Encadrant
* Amaury Dubois

### Développeur
* Zohra Harrat 
* Marc-antoine Lenoir 
* Théo Bouche
* Jimmy Denis
* Antoine Steichen
* Arthur Detant
* Benoît Ledet
* Thomas Boitel