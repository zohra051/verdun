<?php
include("header.php");
if ($droit <1 || $droit >2){header('Location: accueil.php');}
?>

<?php
require_once('connexionBDD.php');
$base = mysqli_connect ($host, $user, $pass,$bdd);
$requete = "SELECT * FROM utilisateurs ORDER BY UTI_Score DESC ";
$result = mysqli_query($base,$requete) or die ('Erreur SQL !<br />'.$sql.'<br />'.mysqli_error());
?>

<div class="container">
<table class="valid table">
    <caption> Classement </caption>
    <tr>
        <th>Rang</th>
        <th>Pseudo</th>
        <th>Score</th>
    </tr> 
	<?php
    $count = 0;
    $array_Id = array();
    $array_Pseudo = array();
    $array_Score = array();
	
    while ($row = mysqli_fetch_array($result))
    {
        array_push($array_Id,$row['UTI_Id']);
        array_push($array_Pseudo,$row['UTI_Pseudo']);
        array_push($array_Score,$row['UTI_Score']);
        $count++;
    }

    for ($i = 0; $i < count($array_Id); $i++)
    {
        $idalerte =$array_Id[$i];
        ?>
        <tr>
            <th><?php echo $i +1; ?> </th>
            <td><?php echo $array_Pseudo[$i]; ?></td>
            <td><?php echo $array_Score[$i]; ?></td>
        </tr>
        <?php
    }
    ?>
</table>
</div>
