class Engine {

    constructor(){
        this.initGame();
    }

    parseMessage(message) {
        let composants = message.split(",");
        //check si message a le bon entète
        if(composants[0] === "T") {
            //on test si le message est du bon type
            let regex = RegExp(/\d+$/);
            if(regex.test(composants[1])) {
                //on test si le coup est possible
                let coup_valide;
                if(composants[2] === "0") {
                    coup_valide = this.coupFr(parseInt(composants[1]));
                }
                else {
                    coup_valide = this.coupAll(parseInt(composants[1]));
                }
                if(coup_valide) {
                    if(this.attente)
                    {
                        return "O,attente"
                    }
                    else {
                        let gagnant_Round = this.checkVictoryRound();
                        this.checkVictoryGame();
                        let gagnant_partie = this.get_winner_game();
                        let nb_soldat_fr = this.get_nb_soldat_fr();
                        let tranchee_fr = this.get_tranchee_fr();
                        let nb_soldat_all = this.get_nb_soldat_all();
                        let tranchee_all = this.get_tranchee_all();
                        let score_fr = this.get_score_fr();
                        let score_all = this.get_score_All();
                        let score_round = this.get_score_round();
                        let intention_fr=this.get_intention_fr();
                        let intention_all=this.get_intention_All();
                        this.resetIntention();
                        this.attente = true;
                        return "E," + gagnant_partie + "," + gagnant_Round + "," + nb_soldat_fr + "," + tranchee_fr + ","+ intention_fr + "," + nb_soldat_all + "," + tranchee_all + ","+intention_all+"," + score_fr + "," + score_all + "," + score_round;
                    }
                }
                //si coup non valide on retourne erreur
                else {
                    return "X,nombre de soldats incorrect" ;
                }
            }
            //si message du mauvais type erreur
            else {
                return "X,pas un nombre ";
            }
        }
        //si message avec mauvais entète, erreur
        else {
            return "X,erreur";
        }
    }


    initGame(){
        this.tranchee_fr = 3;
        this.tranchee_all = 3;
        this.nb_soldat_fr = 100;
        this.nb_soldat_all = 100;
        this.score_fr = 0;
        this.score_all = 0;
        this.nbRound = 0;
        this.winnerGame = -1;
        this.score_round = 0;
        this.intentionFr = -1;
        this.intentionAll = -1;
        this.attente = true;
    }

    initRound(){
        this.tranchee_fr = 3;
        this.tranchee_all = 3;
        this.nb_soldat_fr = 100;
        this.nb_soldat_all = 100;
    }

    coupFr(nbSoldat){
        if(nbSoldat > this.nb_soldat_fr || nbSoldat < 0){
            return false;
        }
        this.intentionFr = nbSoldat;
        if(this.intentionAll !== -1){
            this.coup(this.intentionFr, this.intentionAll);
        }
        return true;
    }

    coupAll(nbSoldat){
        if(nbSoldat > this.nb_soldat_all || nbSoldat < 0){
            return false;
        }
        this.intentionAll = nbSoldat;
        if(this.intentionFr !== -1){
            this.coup(this.intentionFr, this.intentionAll);
        }
        return true;
    }

    resetIntention(){
      this.intentionFr = -1;
      this.intentionAll = -1;
    }

    coup(soldat_fr, soldat_all){
        this.attente = false;
        if(soldat_all > soldat_fr){
            this.tranchee_fr--;
            this.tranchee_all++;
        }
        else if(soldat_fr > soldat_all){
            this.tranchee_all--;
            this.tranchee_fr++;
        }
      this.nb_soldat_fr -= soldat_fr;
      this.nb_soldat_all -= soldat_all;
    }

    checkVictoryRound() {
        if (this.tranchee_fr === -1) { //victoire ecrasante all
            this.score_all += 3;
            this.score_round = 3;
            this.nbRound++;
            this.initRound();
            return 1;
        } else if (this.tranchee_all === -1) { //victoire ecrasante fr
            this.score_fr += 3;
          this.score_round = 3;
            this.nbRound++;
            this.initRound();
            return 0;
        } else if(this.nb_soldat_all === 0 && this.nb_soldat_fr === 0)
        {
            if(this.tranchee_all > this.tranchee_fr){
              this.score_all += 1;
              this.score_round = 1;
              this.nbRound++;
              this.initRound();
              return 1;
            }
            else if (this.tranchee_fr > this.tranchee_all)
            {
              this.score_fr += 1;
              this.score_round = 1;
              this.nbRound++;
              this.initRound();
              return 0;
            }
            else
            {
              this.nbRound++;
              this.score_round = 1;
              this.initRound();
              return 2;
            }
        }
        else {
            return -1;
        }
    }

    checkVictoryGame(){
        if(this.nbRound >= 3) {
            if (this.score_fr > this.score_all) {
                this.winnerGame = 0;
            } else if (this.score_all > this.score_fr) {
                this.winnerGame = 1;
            }
            else if (this.score_fr === this.score_all) {
                this.winnerGame = 2;
            }
        }
    }

    get_tranchee_fr(){
        return this.tranchee_fr;
    }

    get_tranchee_all(){
        return this.tranchee_all;
    }

    get_score_fr(){
        return this.score_fr;
    }

    get_score_All(){
        return this.score_all;
    }

    get_nb_soldat_fr(){
        return this.nb_soldat_fr;
    }

    get_nb_soldat_all(){
        return this.nb_soldat_all;
    }

    get_winner_game(){
        return this.winnerGame;
    }

    get_score_round(){
        let temp=0;
        if(this.score_round > 0) {
            temp = this.score_round;
            this.score_round = 0
        }
        return temp;
    }

    get_intention_fr(){
        return this.intentionFr;
    }

    get_intention_All(){
        return this.intentionAll;
      }
}

module.exports = Engine;