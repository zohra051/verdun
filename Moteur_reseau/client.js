let information;
let camp;
var connection = new WebSocket('ws://51.77.203.240:8080');
connection.onopen = function () {
    connection.send("C,0");
};



connection.onerror = function (error) {
  // just in there were some problems with connection...
  content.html($('<p>', {
   text: 'Sorry, but there\'s some problem with your '
       + 'connection or the server is down.'
    }));
  };

connection.onmessage = function (message) {
  information = message.data.split(',');
  //A la connection des deux personne
  if (information[0] === 'P') {
    if (information[1] === '0') {//le joueur est la france
		
      appear(0);
      desappear(1);
      camp = 0;
      mes("Vous êtes la France");
    }
    if (information[1] === '1') {//le joueur est l'allemagne
      appear(1);
      desappear(0);
      camp = 1;
      mes("Vous êtes l'Allemagne");
    }
  }

  //Message de mise en attente
  if (information[0] === 'O') {
	  
    mes(information[1]);
    desappear(camp);
  }

  //Message d'erreur
  if (information[0] === 'X') {
    mes(information[1]);
  }

  //Message d'état
  if (information[0] === 'E') {
	  desappear(camp);
    mes(""); //fin mise en attente/suppression message d'erreur
    if(information[2] === -1)
		front(parseInt(information[4])-1,information[5],information[8]); //intention de la france et allemagne
	else
		front_sans_deplacement(information[5],information[8]);
	son_avant();
	son_feu();
    sleep(3000).then(resultat_bataille); //attente de 500
  }

};

function resultat_bataille()
{
  if(parseInt(information[2]) >= 0) //si round terminer
  {
    if(parseInt(information[2]) === 0)
    {
      mes(" La France est Victorieuse !! <br> Elle gagne "+information[11]+" points");
      cercle(0,parseInt(information[11]));
    }
    if(parseInt(information[2]) === 1)
    {
      mes(" L'Allemagne est Victorieuse !! <br> Elle gagne "+information[11]+" points");
      cercle(1,parseInt(information[11]));
    }
    if(parseInt(information[2]) === 2)
    {
      mes("Match nul");
      cercle(2,0);
    }
    if(parseInt(information[1]) >= 0) //la partie est gagner
    {
      sleep(1000).then(resultat_partie);
    }
  }
  maj_terrain();
}

function maj_terrain()
{
  appear(camp);
  front(parseInt(information[4]),0,0); //remise a 0 intention fr
  soldat_restant(0,information[3]); // mise a jour nb soldat fr
  soldat_restant(1,information[6]); // mise a jour nb soldat all
  barre(parseInt(information[4]),parseInt(information[7])); //mise a jour trancher all
}

function resultat_partie() {
  if(parseInt(information[1]) === 0)
  {
	 
    mes("La France gagne la guerre !");
    accueil_here();
  }
  if(parseInt(information[1]) === 1)
  {
    mes("L'Allemagne gagne la guerre !");
    accueil_here();
  }
  if(parseInt(information[1]) === 2)
  {
	  
    mes("La guerre continue !");
    accueil_here();
  }
  if(information[1] >= 0)
  {
	  
	  if(information[1] === camp)
	  {
		  son_victoire();
		 }
		 else
		 {
			 son_perte();
			}
	}
}

function sendMessage() {
	if(camp === 0) // envoie message france
	{
		connection.send("T," + document.getElementById("FrField").value);
	}
	else //envoie message allemagne
	{
		connection.send("T," + document.getElementById("AllField").value);		
	}
	son_pas();

}

function wait(ms)
{
  var d = new Date();
  var d2 = null;
  do { d2 = new Date(); }
  while(d2-d < ms);
}

function sleep(ms) {
  return new Promise(resolve => setTimeout(resolve, ms));
}

