const Engine = require('./Engine.js');
const AI = require('./client_IA');
const WebSocket = require('ws');

//Pourrait poser probleme au deploiement
const wss = new WebSocket.Server({ port: 8080 });

let games = [];
let waiting_game = -1;
let awaiting_ai = false;

wss.on('connection', ws => {
    //normalGameConnection(ws);

  ws.on('message', message => {
      const game_number = getGameNumber(ws);
      if(game_number !== -1) {
          let game = games[game_number];
          let envoyer = game[0].parseMessage(message + "," + (game.indexOf(ws) - 1));
          let composants = envoyer.split(",");
          if (composants[0] === 'E') {
              game[1].send(envoyer);
              game[2].send(envoyer);
              if (parseInt(composants[1]) >= 0) {
                  game[1].close();
                  game[2].close();
                  removeGame(game_number);
              }
          }
          else
              ws.send(envoyer);
      }
      else {
          if(message[0] === "C")
          {
              if(parseInt(message.split(",")[1]) === 0) {
                  normalGameConnection(ws);
              }
              else {
                  aiGameConnection(ws);
              }
          }
          else if(message[0] === "A" && awaiting_ai) {
              connectAi(ws);

          }
          else {
              ws.send("X,vous ne faite pas partie d'une partie en cours");
          }
      }
  });

  ws.on('close', disconnect => {
      const game_number = getGameNumber(ws);
      if(game_number !== -1) {
          let game = games[game_number];
          if (game.length === 3) {
              if (game.indexOf(ws) === 1) {
                  game[2].send("X,la France a capitulé : L'Allemagne est victorieuse");
                  game[2].close();
              }
              else {
                  game[1].send("X,l'Allemagne a capitulé : La France est victorieuse");
                  game[1].close();
              }
          }
          removeGame(game_number);
      }
  });
});

function getGameNumber(ws) {
    for(let i = 0; i < games.length; ++i) {
        if(games[i].includes(ws)) {
            return i;
        }
    }
    return -1;
}

function removeGame(game_number) {
    games.splice(game_number,1);
    if(waiting_game > game_number) {
        waiting_game--;
    } else if(waiting_game === game_number) {
        waiting_game = -1;
    }
}

function normalGameConnection (ws) {
    if(waiting_game === -1) {
        let game = [new Engine(), ws];
        games.push(game);
        waiting_game = games.length - 1;
    }
    else {
        const game = games[waiting_game];
        game.push(ws);
        game[1].send("P,0");
        game[2].send("P,1");
        waiting_game = -1;
    }
}

function aiGameConnection(ws) {
    let game = [new Engine(), ws];
    games.push(game);
    awaiting_ai = true;
    new AI();
}

function connectAi(ws) {
    let game = games[games.length - 1]
    game.push(ws);
    game[1].send("P,0");
    game[2].send("P,1");
    awaiting_ai =false;
}

