<?php
session_start();
if (!isset($_SESSION['login']))
{
	header ('Location: index.php');
	exit();
}
?>

<html>
	<head>
		<title>Espace membre</title>
		<link rel="stylesheet" href="css/espace-membre.css" type="text/css"/>
		<meta charset="utf-8"/>
	</head>
	<body>
		<?php
		header('Location: accueil.php');
		?>
	</body>
</html>