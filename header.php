<!doctype html>
<html>
	<head>
		<title>Verdun</title>
		<link rel="stylesheet" href="css/espace-membre.css" type="text/css"/>
		<link rel="stylesheet" type="text/css" href="css/menu.css">
		<meta charset="utf-8"/>
		<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
		<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css" integrity="sha384-BVYiiSIFeK1dGmJRAkycuHAHRg32OmUcww7on3RYdg4Va+PmSTsz/K68vbdEjh4u" crossorigin="anonymous">
		<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap-theme.min.css" integrity="sha384-rHyoN1iRsVXV4nD0JutlnGaslCJuC7uwjduW9SVrLvRYooPp2bWYgmgJQIXwl/Sp" crossorigin="anonymous">
		<script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js" integrity="sha384-Tc5IQib027qvyjSMfHjOMaLkfuWVxZxUPnCJA7l2mCWNIpG9mGCD8wGNIcPD7Txa" crossorigin="anonymous"></script>
	</head>
</html>

<div class="extraheader">
	<?php session_start();
	if (!isset($_SESSION['login']))
	{
		?>
		<center> <img src="img/verdun2.jpg" /> </center>
		<?php
		$droit = 0;
	}
	else
	{
        require("info.php");
		?>
        <img src="img/verdun2.jpg" />
        <div class="bonjour">
			<img src="img/verd.png" style="text-align : center; width : 50%;" />
			</br>
            <?php echo $data['UTI_Pseudo']; ?> 
			</br>
			<a href="deconnexion.php"><img src="img/logout1.png" class="deco" height="20%" width="20%" /></a>
        </div>
		
        <?php
        $donnees = $_SESSION['login'];
		require_once('connexionBDD.php');
		$base = mysqli_connect ($host, $user, $pass,$bdd);
        $sql = "SELECT UTI_Droit FROM utilisateurs WHERE UTI_Pseudo = '$donnees'";
        $req = mysqli_query($base,$sql) or die('Erreur SQL !<br />'.$sql.'<br />'.mysqli_error());
        $data = mysqli_fetch_array($req);
        mysqli_free_result ($req);
        mysqli_close ($base);
        $droit = $data['UTI_Droit'];
	} 
	?>
</div>
<div class="header">
	<?php
	if ($droit == 2) // admin
	{
		?>
		<div class="container">
			<div class="masthead">
				<h3 class="text-muted">Verdun</h3>
				<nav>
					<ul class="nav nav-justified">
						<li><a href="accueil.php">Accueil</a></li>
						<li><a href="jeu_local.html">Jeu en local</a></li>
						<li><a href="interface/jeu_reseau.html">Jeu en ligne</a></li>
						<li><a href="interface/jeu_reseau_IA.html">Jeu contre IA</a></li>
						<li><a href="classement.php">Classement</a></li>
					</ul>
				</nav>
			</div>
		</div>
		
		<?php
	}
	else
	{
		if ($droit == 1) // utilisateur
		{
			?>
			<div class="container">
				<div class="masthead">
					<h3 class="text-muted">Verdun</h3>
					<nav>
						<ul class="nav nav-justified">
							<li><a href="accueil.php">Accueil</a></li>
							<li><a href="jeu_local.html">Jeu en local</a></li>
							<li><a href="interface/jeu_reseau.html">Jeu en ligne</a></li>
							<li><a href="interface/jeu_reseau_IA.html">Jeu contre IA</a></li>
							<li><a href="classement.php">Classement</a></li>
						</ul>
					</nav>
				</div>
			</div>
			<?php
		}
		else
		{
			?>
			<div class="container">
				<div class="masthead">
					<h3 class="text-muted">Verdun</h3>
					<nav>
						<ul class="nav nav-justified">
							<li><a href="inscription.php">Non Inscrit !</a></li>
						</ul>
					</nav>
				</div>
			</div>
			<?php
		}
	}
	?>
</div>