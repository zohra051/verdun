<html>
	<head>
		<title>Verdun</title>
		<link rel="stylesheet" href="css/espace-membre.css" type="text/css"/>
		<meta charset="utf-8"/>
		<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css" integrity="sha384-BVYiiSIFeK1dGmJRAkycuHAHRg32OmUcww7on3RYdg4Va+PmSTsz/K68vbdEjh4u" crossorigin="anonymous">
		<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap-theme.min.css" integrity="sha384-rHyoN1iRsVXV4nD0JutlnGaslCJuC7uwjduW9SVrLvRYooPp2bWYgmgJQIXwl/Sp" crossorigin="anonymous">
		<script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js" integrity="sha384-Tc5IQib027qvyjSMfHjOMaLkfuWVxZxUPnCJA7l2mCWNIpG9mGCD8wGNIcPD7Txa" crossorigin="anonymous"></script>
	</head>
	
	<body>
		<center><img src="img/verdun.jpg" /></center>
		<div class="bande" id="couleur">
			<div class="textebande">
				Veuillez-vous connecter pour acceder au site
			</div>
		</div>
		
		<?php
		// on teste si le visiteur a soumis le formulaire
		if (isset($_POST['inscription']) && $_POST['inscription'] == 'inscription')
		{
			// on teste l'existence de nos variables. On teste également si elles ne sont pas vides
			if ((isset($_POST['login']) && !empty($_POST['login'])) && (isset($_POST['pass']) && !empty($_POST['pass'])) && (isset($_POST['pass_confirm']) && !empty($_POST['pass_confirm'])))
			{
				// on teste les deux mots de passe
				if ($_POST['pass'] != $_POST['pass_confirm'])
				{
					$erreur = 'Les 2 mots de passe sont différents.';
				}
				else
				{
					require_once('connexionBDD.php');
					$base = mysqli_connect ($host, $user, $pass,$bdd);
					
					// on recherche si ce login est déjà utilisé par un autre membre
					$sql = 'SELECT count(*) FROM utilisateurs WHERE UTI_Pseudo="'.mysqli_real_escape_string($base,$_POST['login']).'"';
					$req = mysqli_query($base,$sql) or die('Erreur SQL !<br />'.$sql.'<br />'.mysqli_error($base));
					$data = mysqli_fetch_array($req);
					
					if ($data[0] == 0)
					{
						$sql = 'INSERT INTO utilisateurs VALUES("0", "'.mysqli_real_escape_string($base,$_POST['login']).'", "'.mysqli_real_escape_string($base,md5($_POST['pass'])).'","100","1")';
						mysqli_query($base,$sql) or die('Erreur SQL !'.$sql.'<br />'.mysqli_error($base));
						session_start();
						$_SESSION['login'] = $_POST['login'];
						header('Location: membre.php');
						exit();
					}
					else
					{
						$erreur = 'Un membre possède déjà ce login.';
					}
				}
			}
			else
			{
				$erreur = 'Au moins un des champs est vide.';
			}
		}
		?>

		<h1>Bienvenue sur le jeu Verdun</h1><br>
		<div class="container">
			<div class="row">
				<div class="col-md-3 ">
				</div>
				<div  class="col-md-6 ">
					<form action="inscription.php" method="post"  class="form-signin">
						<h2 class="form-signin-heading">Inscription au jeu Verdun</h2>
						<input type="name"  class="form-control" placeholder="Pseudo" name="login" value="<?php if (isset($_POST['login'])) echo htmlentities(trim($_POST['login'])); ?>" REQUIRED>
						<input type="password"  class="form-control" placeholder="Mot de passe" name="pass" value="<?php if (isset($_POST['pass'])) echo htmlentities(trim($_POST['pass'])); ?>" REQUIRED>
						<input type="password"  class="form-control" placeholder="Mot de passe" name="pass_confirm" value="<?php if (isset($_POST['pass_confirm'])) echo htmlentities(trim($_POST['pass_confirm'])); ?>" REQUIRED>
						<br>
						<button type="submit" name="inscription" value="inscription" class="btn btn-lg btn-default btn-block " >Inscription</button>
					</form>
					<form action="index.php">
						<button class="btn btn-lg btn-default btn-block" type="submit">
							Retour à la page de connexion
						</button>
					</form>
					
					<?php
					if (isset($erreur))
					{
						echo '<br />',$erreur;
					}
					?>
				</div>
				<div class="col-md-3 ">
				</div>
			</div>
		</div>
	
		<?php
		include("footer.php");
		?>
		
	</body>
</html>