var eng = new Engine();;

var nb_round=1;
var ok1 = false;
var ok2 = false;

var barre = function (bleu,rouge) 
{
	var fix = bleu+1;
	var b1 = document.getElementById("1");
	var b2 = document.getElementById("2");
	var b3 = document.getElementById("3");
	var b4 = document.getElementById("4");
	var b5 = document.getElementById("5");
	var b6 = document.getElementById("6");
	var i=1;
	var tab=[b1,b2,b3,b4,b5,b6];
	
	do{ //tableau commence a 0
		var decalage=20;
		if (i<=bleu){
		
			tab[i-1].src="img/tranchee_bleue.png";
			var bouge = document.getElementById('epee');
			bouge.innerHTML =' <img class="epee'+fix+'" src="img/epee.png" style="position:absolute;z-index:6;"/>';
			
			
		}else{
			tab[i-1].src="img/tranchee_rouge.png";

		}i++;
	} while(i<=6)
}

var cercle = function(camp,couleur)
{
    var nb=0+nb_round;
    var nb2=3+nb_round;
    /*France 1 point */
    if(camp === 0 && couleur === 1)
    {
        var cercle = document.getElementById("id_cercle"+nb);
        cercle.innerHTML="<div class='cercle_jaune'></div>";
        var cercle2 = document.getElementById("id_cercle"+nb2);
        cercle2.innerHTML="<div class='cercle_rouge'></div>";

    }

    /*France 3 point*/
    if(camp === 0 && couleur === 3)
    {
        var cercle = document.getElementById("id_cercle"+nb);
        cercle.innerHTML="<div class='cercle_vert'></div>";
        var cercle2 = document.getElementById("id_cercle"+nb2);
        cercle2.innerHTML="<div class='cercle_rouge'></div>";
    }

    /*Allemagne 1 point*/
    if(camp === 1 && couleur === 1)
    {
        var cercle = document.getElementById("id_cercle"+nb2);
        cercle.innerHTML="<div class='cercle_jaune'></div>";
        var cercle2 = document.getElementById("id_cercle"+nb);
        cercle2.innerHTML="<div class='cercle_rouge'></div>";
    }

    /*Allemagne 3 point*/
    if(camp === 1 && couleur === 3)
    {
        var cercle = document.getElementById("id_cercle"+nb2);
        cercle.innerHTML="<div class='cercle_vert'></div>";
        var cercle2 = document.getElementById("id_cercle"+nb);
        cercle2.innerHTML="<div class='cercle_rouge'></div>";
    }

    if(camp === 2 )
    {
        var cercle = document.getElementById("id_cercle"+nb);
        cercle.innerHTML="<div class='cercle_rouge'></div>";
        var cercle2 = document.getElementById("id_cercle"+nb2);
        cercle2.innerHTML="<div class='cercle_rouge'></div>";
    }
    nb_round++;

}

function reset_rond(){
    var i;
    for(i = 1; i<= 6; i++){
        var cercle = document.getElementById("id_cercle"+i);
        cercle.innerHTML = "<div class = 'cercle_blanc'></div>";
        nb_round=1;
    }
}


// open connection

var connection = new WebSocket('ws://127.0.0.1:8080');
connection.onopen = function () {

};

/*connection.onerror = function (error) {
    // just in there were some problems with connection...
    content.html($('<p>', {
        text: 'Sorry, but there\'s some problem with your '
        + 'connection or the server is down.'
    }));
};*/

connection.onmessage = function (message) {
    var para = document.createElement("p");
    var node = document.createTextNode("ping");
    para.appendChild(node);
    var element = document.body;
    element.appendChild(para);
    //console.log(message);
    //connection.send('pong');
};

console.log(eng.tranchee_all);


document.getElementById('envoyer1').addEventListener('click', getValue1);
document.getElementById('envoyer2').addEventListener('click', getValue2);
document.getElementById('fight').addEventListener('click', fight);


function afficherSoldatRestantFr(){
    var ecriture = document.getElementById('nbSoldatj1R');
    ecriture.innerHTML="";
    ecriture.innerHTML = eng.nb_soldat_fr;
}

function afficherSoldatRestantAll(){
    var ecriture = document.getElementById('nbSoldatj2R');
    ecriture.innerHTML="";
    ecriture.innerHTML = eng.nb_soldat_all;
}



function getValue1(){
    var error;
    var value = document.getElementById('valeur1');
    var x = value.value;
    var numero = parseInt(x, 10);



    if(numero>eng.nb_soldat_fr || numero < 0){
        error = document.getElementById("error1");
        error.innerHTML = "Vous n'avez pas assez de soldats ! Veuillez ressaisir !";
        return;
    }else{
        ok1=true;
        error = document.getElementById("error1");
        error.innerHTML = "";
    }


    eng.coupFr(numero);
    connection.send(numero);
}

function getValue2(){
    var error;
    var value = document.getElementById('valeur2');
    var x = value.value;
    var numero = parseInt(x, 10);



    if(numero>eng.nb_soldat_all || numero < 0){
        error = document.getElementById("error2");
        error.innerHTML = "Vous n'avez pas assez de soldats ! Veuillez ressaisir !";
        return;
    }else{
        ok2=true;
        error = document.getElementById("error2");
        error.innerHTML = "";
    }
    eng.coupAll(numero);
    connection.send(numero);
}

function afficherUpdate(){
    var ecriture = document.getElementById('nbSoldatj1E');
    ecriture.innerHTML = "";
    ecriture.innerHTML = eng.intentionFr;
    afficherSoldatRestantFr();

    ecriture = document.getElementById('nbSoldatj2E');
    ecriture.innerHTML = "";
    ecriture.innerHTML = eng.intentionAll;
    afficherSoldatRestantAll();

    barre(eng.tranchee_fr, eng.tranchee_all);
    var clear = document.getElementById("valeur1");
    clear.value="";
    clear = document.getElementById("valeur2");
    clear.value="";
    afficherRound();
}

function fight(){
    var message = document.getElementById("attente");
    var winner_round = -1;
    if(ok1 === false || ok2===false){
        message.innerHTML = "En attente de tous les joueurs !";
    }else {
        eng.coup(eng.intentionFr, eng.intentionAll);
        message.innerHTML = "";
        afficherUpdate();
        eng.intentionAll = -1;
        eng.intentionFr = -1;
        winner_round = eng.checkVictoryRound();

        if (winner_round !== -1) {
            cercle(winner_round, eng.score_round);
            eng.initRound();
            var v = document.getElementById("victoire");
            v.innerHTML = "";

            console.log("winner round", winner_round);

            if(winner_round === 0){
                v.innerHTML = "Round pour la France !";
            }
            else if(winner_round === 1){
                v.innerHTML = "Round pour l'Allemagne !";
            }
            else if(winner_round === 2){
                v.innerHTML = "Round Nul !";
            }

            setTimeout(function(){ v.innerHTML = ""; }, 3000);



            console.log("nbRound :", eng.nbRound);
            eng.checkVictoryGame();

            if(eng.nbRound !== 3){
                eng.nbRound++;
            }



            if(eng.winnerGame === 0){
                v.innerHTML = "MATCH NUL !";
                setTimeout(function(){ v.innerHTML = ""; }, 3000);
            }else if(eng.winnerGame === 1){
                v.innerHTML = "VICTOIRE POUR LA FRANCE !";
                setTimeout(function(){ v.innerHTML = ""; }, 3000);
            }else if(eng.winnerGame === 2){
                v.innerHTML = "VICTOIRE POUR L'ALLEMAGNE !";
                setTimeout(function(){ v.innerHTML = ""; }, 3000);
            }

            if(eng.winnerGame !== -1){
                eng.initGame();
                init();
                reset_rond();
            }


            afficherUpdate();


            var ecriture = document.getElementById('nbSoldatj1E');
            ecriture.innerHTML = "";
            ecriture = document.getElementById('nbSoldatj2E');
            ecriture.innerHTML = "";

            console.log("engin :", eng);


        }

    }
    ok1=false;
    ok2=false;
}

function afficherRound(){
    var ecriture = document.getElementById('nbRound');
    ecriture.innerHTML = eng.nbRound;
}

function init(){
    afficherRound();
    afficherSoldatRestantAll();
    afficherSoldatRestantFr();
}

function sleep(ms) {
    return new Promise(resolve => setTimeout(resolve, ms));
}


window.onload=init;
































