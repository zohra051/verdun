class Engine {
    constructor(){
        this.initGame();
        this.intentionFr = -1;
        this.intentionAll = -1;
    }

    initGame(){
        this.tranchee_fr = 3;
        this.tranchee_all = 3;
        this.nb_soldat_fr = 100;
        this.nb_soldat_all = 100;
        this.score_fr = 0;
        this.score_round = -1;
        this.score_all = 0;
        this.nbRound = 1;
        this.winnerGame = -1;
    }

    initRound(){
        this.tranchee_fr = 3;
        this.tranchee_all = 3;
        this.nb_soldat_fr = 100;
        this.nb_soldat_all = 100;
    }

    coupFr(nbSoldat){
        this.intentionFr = nbSoldat;
    }

    coupAll(nbSoldat){
        this.intentionAll = nbSoldat;
    }

    coup(soldat_fr, soldat_all){
        this.nb_soldat_fr -= soldat_fr;
        this.nb_soldat_all -= soldat_all;

        if(soldat_all > soldat_fr){
            this.tranchee_fr--;
            this.tranchee_all++;
        }else if(soldat_fr > soldat_all){
            this.tranchee_all--;
            this.tranchee_fr++;
        }
    }

    checkVictoryRound() {
        if (this.tranchee_fr === -1) { //victoire ecrasante all
            this.score_all += 3;
            this.score_round = 3;
            //this.initRound();
            return 1;
        } else if (this.tranchee_all === -1) { //victoire ecrasante fr
            this.score_fr += 3;
            this.score_round = 3;
            //this.initRound();
            return 0;
        } else if(this.nb_soldat_all === 0 && this.nb_soldat_fr === 0){
            if(this.tranchee_all > this.tranchee_fr){
                this.score_all += 1;
                this.score_round = 1;
               // this.initRound();
                return 1;
            } else if (this.tranchee_fr > this.tranchee_all){
                this.score_fr += 1;
                this.score_round = 1;
               // this.initRound();
                return 0;
            } else {
                this.score_round = 1;
               // this.initRound();
                return 2;
            }
        }
        else {
            return -1;
        }

    }

    checkVictoryGame(){
        if(this.nbRound===3 && (this.score_fr > this.score_all)){
            this.winnerGame = 1;
        }else if(this.nbRound === 3 && (this.score_all > this.score_fr)){
            this.winnerGame = 2;
        }else if(this.nbRound === 3 && (this.score_all === this.score_fr)){
            this.winnerGame = 0;
        }
    }

    set_tranchee_fr(i){
        this.tranchee_fr = i;
    }

    set_tranchee_all(i){
        this.tranchee_all = i;
    }

    get_tranchee_fr(){
        return this.tranchee_fr;
    }

    get_tranchee_all(){
        return this.tranchee_all;
    }
}

//export {Engine};