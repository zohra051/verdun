const WebSocket = require('ws');

class AI {
    constructor() {
        let nb_soldats =100;
        let side = -1;

        let connection = new WebSocket('ws://51.77.203.240:8080');

        connection.onopen = function () {
            connection.send("A");
        };

        connection.onerror = function (error) {
        };

        connection.onmessage = function (message) {
            const information = message.data.split(",");
            _play(information);
        };

        function _play(information) {
            if(information[0] === "P") {
                side = parseInt(information[1]);
                connection.send("T," + _getRandomNumber())
            }
            else if(information[0] === "E" && parseInt(information[1]) === -1) {
                if (side === 0) {
                    nb_soldats = parseInt(information[3]);
                    connection.send("T," + _getRandomNumber());
                } else if (side === 1) {
                    nb_soldats = parseInt(information[6]);
                    connection.send("T," + _getRandomNumber());
                }
                else {
                    console.log("Erreur IA : camp non defini");
                }
            }
        }

        function _getRandomNumber() {
            return Math.ceil(Math.random() * nb_soldats);
        }
    }
}

module.exports = AI;
