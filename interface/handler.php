<?php
//Connexion à la base de données
$db = new PDO('mysql:host=127.0.0.1;dbname=verdun;charset=utf8', 'root', 'verdunulco', [
    PDO::ATTR_ERRMODE => PDO::ERRMODE_EXCEPTION, 
    PDO::ATTR_DEFAULT_FETCH_MODE => PDO::FETCH_ASSOC
]);

//On doit analyser la demande faite via l'URL (GET) afin de déterminer si on souhaite récupérer les messages ou en écrire un
$task = "list";

if(array_key_exists("task", $_GET)){
  $task = $_GET['task'];
}

if($task == "write"){
  postMessage();
} else {
  getMessages();
}

//Si on veut récupérer, il faut envoyer du JSON
function getMessages(){
  global $db;
  
  $resultats = $db->query("SELECT * FROM messages ORDER BY created_at DESC LIMIT 20");
  $messages = $resultats->fetchAll();
  
  echo json_encode($messages);
}

//Si on veut écrire au contraire, il faut analyser les paramètres envoyés en POST et les sauver dans la base de données
function postMessage(){
  global $db;
  $author = "Joueur";
  $content = $_POST['content'];
  
  $query = $db->prepare('INSERT INTO messages SET author = :author, content = :content, created_at = NOW()');
  
  $query->execute([
    "author" => $author,
    "content" => $content
  ]);

  echo json_encode(["status" => "success"]);
  
}
?>
