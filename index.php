<!doctype html>
<html>
	<head>
		<title>Verdun</title>
		<link rel="stylesheet" href="css/espace-membre.css" type="text/css"/>
		<meta charset="utf-8"/>
		<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css" integrity="sha384-BVYiiSIFeK1dGmJRAkycuHAHRg32OmUcww7on3RYdg4Va+PmSTsz/K68vbdEjh4u" crossorigin="anonymous">
		<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap-theme.min.css" integrity="sha384-rHyoN1iRsVXV4nD0JutlnGaslCJuC7uwjduW9SVrLvRYooPp2bWYgmgJQIXwl/Sp" crossorigin="anonymous">
		<script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js" integrity="sha384-Tc5IQib027qvyjSMfHjOMaLkfuWVxZxUPnCJA7l2mCWNIpG9mGCD8wGNIcPD7Txa" crossorigin="anonymous"></script>
	</head>
	
	<body>
		<?php
		if ((isset($_POST['login']) && !empty($_POST['login'])) && (isset($_POST['pass']) && !empty($_POST['pass'])))
		{
			require_once('connexionBDD.php');
			$base = mysqli_connect ($host, $user, $pass,$bdd);
			// on teste si une entrée de la base contient ce couple login / pass
			$sql = 'SELECT count(*) FROM utilisateurs WHERE UTI_Pseudo="'.mysqli_real_escape_string($base,$_POST['login']).'" AND UTI_MDP="'.mysqli_real_escape_string($base,md5($_POST['pass'])).'"';
			
			$req = mysqli_query($base,$sql) or die('Erreur SQL !<br />'.$sql.'<br />'.mysqli_error());
			$data = mysqli_fetch_array($req);
			
			mysqli_free_result($req);
			mysqli_close($base);
			
			// si on obtient une réponse, alors l'utilisateur est un membre
			if ($data[0] == 1)
			{
				session_start();
				$_SESSION['login'] = $_POST['login'];
				header('Location: membre.php');
				exit();
			}
			
			// si on ne trouve aucune réponse, le visiteur s'est trompé soit dans son login, soit dans son mot de passe
			else if ($data[0] == 0)
			{
				$erreur = 'Compte non reconnu. ';
			}
			else
			{
				$erreur = 'Problème dans la base de données : plusieurs membres ont les mêmes identifiants de connexion.';
			}
		}
		?>
		
		<center><img src="img/verdun.jpg" class="logoverdun" /></center>
		
		<div class="bande" id="couleur">
			<div class="textebande">
				Veuillez vous connecter pour acceder au site
			</div>
		</div>
		
		<h1>Bienvenue sur Verdun</h1><br>
		
		<div class="container">
			<div class="row">
				<div class="col-md-3 ">
				</div>
				<div class="col-md-6 ">
					<form action="index.php" method="post"  class="form-signin">
						<h2 class="form-signin-heading">Connexion au jeu Verdun</h2>
						<input type="text" id="inputEmail" class="form-control" placeholder="Pseudo" name="login" value="<?php if (isset($_POST['login'])) echo htmlentities(trim($_POST['login'])); ?>" REQUIRED>
						<input type="password" id="inputPassword" class="form-control" placeholder="Mot de passe" name="pass" value="<?php if (isset($_POST['pass'])) echo htmlentities(trim($_POST['pass'])); ?>" REQUIRED>
						<br>
						<button type="submit" name="connexion" value="connexion" class="btn btn-lg btn-default btn-block " >Connexion</button>
					</form>
					<br>
					<form action="inscription.php">
						<button class="btn btn-lg btn-default btn-block" type="submit">Inscrivez vous</button>
					</form>
					<?php
					if (isset($erreur)) echo '<br />',$erreur;
					?>
				</div>
				<div class="col-md-3 ">
				</div>
			</div>
		</div>
		
		<?php
		include("footer.php");
		?>
	</body>
</html>
